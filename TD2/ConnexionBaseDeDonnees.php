<?php

class ConnexionBaseDeDonnees
{

    private PDO $pdo;

    private static ?ConnexionBaseDeDonnees $instance = null;

    /**
     * @param PDO $pdo
     */
    private function __construct()
    {
        require_once("ConfigurationBaseDeDonnees.php");
        $login =  ConfigurationBaseDeDonnees::getLogin();
        $nomBaseDeDonnees = ConfigurationBaseDeDonnees::getNomBaseDeDonnees();
        $port = ConfigurationBaseDeDonnees::getPort();
        $nomHote = ConfigurationBaseDeDonnees::getNomHote();
        $motDePasse = ConfigurationBaseDeDonnees::getPassword();
        $this->pdo = new PDO("mysql:host=$nomHote;port=$port;dbname=$nomBaseDeDonnees", $login, $motDePasse,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        ;
    }

    public static function getPdo(): PDO
    {
        return ConnexionBaseDeDonnees::getInstance()->pdo;
    }

    private static function getInstance() : ConnexionBaseDeDonnees {
        // L'attribut statique $instance s'obtient avec la syntaxe ConnexionBaseDeDonnees::$instance
        if (is_null(ConnexionBaseDeDonnees::$instance))
            // Appel du constructeur
            ConnexionBaseDeDonnees::$instance = new ConnexionBaseDeDonnees();
        return ConnexionBaseDeDonnees::$instance;
    }


}