<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\ModeleUtilisateur;
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::getUtilisateurs();
        $values = [
            "utilisateurs" => $utilisateurs,
            "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/liste.php"
        ];
        self::afficherVue('vueGenerale.php',$values);
    }

    public static function afficherDetail() : void{
        $login = $_GET['login'];
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login);
        if($utilisateur != null) {
            $values = [
                "utilisateur" => $utilisateur,
                "titre" => "Detail Utilisateur",
                "cheminCorpsVue" => "utilisateur/detail.php"
            ];
            self::afficherVue("vueGenerale.php",$values);
        }
        else{
            $values = [
                "titre" => "Page d'Erreur",
                "cheminCorpsVue" => "utilisateur/erreur.php"
            ];
            self::afficherVue("vueGenerale.php",$values);
        }
    }

    public static function afficherFormulaireCreation() : void{
        $values = [
            "titre" => "Formulaire Création",
            "cheminCorpsVue" => 'utilisateur/formulaireUtilisateur.php'
        ];
        self::afficherVue("vueGenerale.php",$values);
    }

    public static function creerDepuisFormulaire() : void{
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $utilisateur = new ModeleUtilisateur($login,$nom,$prenom);
        $utilisateur->ajouter();
        $utilisateurs = ModeleUtilisateur::getUtilisateurs();
        $values = [
            "titre" => "Formulaire Création",
            "cheminCorpsVue" => 'utilisateur/utilisateurCree.php',
            "utilisateurs" => $utilisateurs
        ];
        self::afficherVue('vueGenerale.php',$values);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__."/../vue/$cheminVue"; // Charge la vue
    }



}
?>
