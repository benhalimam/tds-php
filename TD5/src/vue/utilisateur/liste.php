<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php
/** @var ModeleUtilisateur[] $parametres["utilisateurs"] */
foreach ($parametres["utilisateurs"] as $utilisateur)
    echo '<p> Utilisateur de login <a href="controleurFrontal.php?action=afficherDetail&login=' .
        rawurlencode($utilisateur->getLogin()).'">' . htmlspecialchars($utilisateur->getLogin()) . '</a></p>';

    echo '<p> <a href="controleurFrontal.php?action=afficherFormulaireCreation"> creer un nouveau utilisateur</a></p>';
?>
</body>
</html>
