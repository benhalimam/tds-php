<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php
/** @var ModeleUtilisateur[] $parametres */
echo '<p> ModeleUtilisateur '.htmlspecialchars($parametres["utilisateur"]->getNom()).' '.htmlspecialchars($parametres["utilisateur"]->getPrenom()).' de login '.htmlspecialchars($parametres["utilisateur"]->getLogin()).' </p>';
?>
</body>
</html>
