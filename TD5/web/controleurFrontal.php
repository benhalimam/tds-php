<?php
require_once __dir__ . "./../src/Lib/Psr4AutoloaderClass.php";
// On récupère l'action passée dans l'URL
$action = $_GET['action'];

$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');


use App\Covoiturage\Controleur\ControleurUtilisateur;


// Appel de la méthode statique $action de ControleurUtilisateur
ControleurUtilisateur::$action();
?>
