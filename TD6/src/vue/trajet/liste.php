<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php
use App\Covoiturage\Modele\DataObject\Trajet;
/** @var \App\Covoiturage\Modele\DataObject\Trajet[] $parametres["Trajets"] */
foreach ($parametres["trajets"] as $trajet) {
    $idHtml = $trajet->getID(); // c'est forcement un nombre , c'est la bd qui la genere
    $departHtml = html_entity_decode($trajet->getDepart());
    $arriverHtml = html_entity_decode($trajet->getArrivee());
    $urlDetail = "controleurFrontal.php?controleur=trajet&action=afficherDetail&id=$idHtml";
    $urlSuppression = "controleurFrontal.php?controleur=trajet&action=supprimer&id=$idHtml";
    $urlModdif = "controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&id=$idHtml";

    echo "<p>le trajet d'identifiant <a href =\"".$urlDetail."\">".$idHtml."</a> part de ".$departHtml." pour ".
        $arriverHtml." <a href=\"".$urlSuppression."\">Supprimer</a>"." <a href=\"".$urlModdif."\">Modifier</a>"."</p>";
}
// en dehors du foreach
echo '<p> <a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireCreation"> creer un nouveau Trajet</a></p>';

?>
</body>
</html>
