<?php

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

class Trajet {

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private Utilisateur $conducteur;
    private bool $nonFumeur;

    /**
     * @var Utilisateur[]
     */
    private array $passagers;



    public function __construct(
        ?int $id,
        string $depart,
        string $arrivee,
        DateTime $date,
        int $prix,
        Utilisateur $conducteur,
        bool $nonFumeur
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = array();
    }

    public static function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arriver"],
            new DateTime($trajetTableau["date"]), // À changer
            $trajetTableau["prix"],
            Utilisateur::recupererUtilisateurParLogin($trajetTableau["conducteurLogin"]), // À changer
            $trajetTableau["nonFumeur"], // À changer ? non
        );
        $trajet->setPassagers($trajet->recupererPassagers());
        return $trajet;
    }

    public function getPassagers(): array
    {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }


    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): Utilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(Utilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        $strUtilisateurs = "";
        foreach ($this->passagers as $passager) {$strUtilisateurs = $strUtilisateurs.$passager->__toString()."<br>";}
        return "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).
            <br>$strUtilisateurs
        </p>";
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public function ajouter() : void{
        $sql = "INSERT INTO `trajet` (`id`, `depart`, `arriver`, `date`, `prix`, `conducteurLogin`, `nonFumeur`) VALUES 
          (NULL, :departTag, :arriverTag, :dateTag, :prixTag, :conducteurLoginTag, :nonFumeurTag)";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "departTag" => $this->depart,
            "arriverTag" => $this->arrivee,
            "dateTag" => $this->date->format("Y-m-d"),
            "prixTag" => $this->prix,
            "conducteurLoginTag" => $this->conducteur->getLogin(),
            "nonFumeurTag" => $this->nonFumeur?1:0
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
    }

    /**
     * @return Utilisateur[]
     */
    private function recupererPassagers() : array {
        $array = array();

        $sql = "select passagerLogin from passager where trajetId = :trajetIdTag";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "trajetIdTag" => $this->id
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        foreach($pdoStatement as $passagerFormatTableau) {
            $array[] = Utilisateur::recupererUtilisateurParLogin($passagerFormatTableau["passagerLogin"]);
        }

        return $array;
    }


}
