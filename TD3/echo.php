<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>

    <body>
        Voici le résultat du script PHP :
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */

          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          echo $texte;
        ?>
        <p>
            <?php
                echo "debut du second php \n";
                $utilisateur0 = ["nom" => "Leblanc" , "prenom" => "Juste" , "login" => "leblancj"];
                $utilisateur1 = ["nom" => "Benhalima" , "prenom" => "Matteo" , "login" => "benhalimam"];
                $utilisateur2 = ["nom" => "Daoudi" , "prenom" => "oussama" , "login" => "daoudio"];
                $utilisateurs = [$utilisateur0, $utilisateur1, $utilisateur2];
                if (empty($utilisateurs)){
                    echo "aucun utilisateur d'inscrit ! ";
                }
                else {
                    echo "<ul>";
                    foreach ($utilisateurs as $utilisateur) {
                        echo "<li> Utilisateur $utilisateur[prenom] $utilisateur[nom] de login $utilisateur[login] </li>";
                    }
                    echo "</ul>";
                }
            ?>
        </p>
    </body>
</html> 