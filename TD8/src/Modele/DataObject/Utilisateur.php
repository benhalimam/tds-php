<?php

namespace App\Covoiturage\Modele\DataObject;

class Utilisateur  extends AbstractDataObject {

    private string $login;
    private string $nom;
    private string $prenom;

    private string $mdpHache;

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getMdpHache(): string
    {
        return $this->mdpHache;
    }

    public function setMdpHache(string $mdpHache): void
    {
        $this->mdpHache = $mdpHache;
    }





    public function __construct(string $login, string $nom, string $prenom, string $mdpHache){
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->mdpHache = $mdpHache;
    }




//    public function __toString() : string {
//        return "<p> Utilisateur {$this->prenom} {$this->nom} de login {$this->login} </p>";
//    }
}