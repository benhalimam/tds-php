<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie{


    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void{
        $dureeExpiration == null ? $dureeExpiration = 0 :$dureeExpiration = $dureeExpiration + time();
        $serialized = serialize($valeur);
        setcookie($cle, $serialized, $dureeExpiration);
    }

    public static function lire(string $cle): mixed{
        isset($_COOKIE[$cle]) ? $valeur = $_COOKIE[$cle] : $valeur = "";
        return unserialize($valeur);
    }

    public static function contient($cle) : bool{
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle) : void{
        setcookie($cle, '', 1);
        unset($_COOKIE[$cle]);
    }








}