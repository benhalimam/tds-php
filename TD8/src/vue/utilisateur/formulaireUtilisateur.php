<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title> Mon premier php </title>
</head>
<body>
<form method="get" action="controleurFrontal.php">
    <input type='hidden' name='action' value='creerDepuisFormulaire'>
    <input type='hidden' name='controleur' value='utilisateur'>
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label for="login_id" class="InputAddOn-item">Login</label> :
            <input class="InputAddOn-field"  type="text" placeholder="leblancj" name="login" id="login_id" required/>
        </p>
        <p class="InputAddOn">
            <label for="prenom_id" class="InputAddOn-item">Prenom</label> :
            <input class="InputAddOn-field"  type="text" placeholder="Juste" name="prenom" id="prenom_id" required />
        </p>
        <p class="InputAddOn">
            <label for="nom_id" class="InputAddOn-item">Nom</label> :
            <input class="InputAddOn-field" type="text" placeholder="Leblanc" name="nom" id="nom_id" required />
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
</body>
</html>
