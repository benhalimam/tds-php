<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php
/** @var \App\Covoiturage\Modele\DataObject\Utilisateur $utilisateur */
use App\Covoiturage\Lib\ConnexionUtilisateur;$utilisateur;
echo '<p> Utilisateur '.htmlspecialchars($utilisateur->getNom()).' '.htmlspecialchars($utilisateur->getPrenom()).' de login '.htmlspecialchars($utilisateur->getLogin()).' </p>';

if (ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())){
    echo '<a href="controleurFrontal.php?controleur=utilisateur&action=supprimer&login=' .
        rawurlencode($utilisateur->getLogin()) . '">'  ." supprimer". '</a>
        <a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' .
        rawurlencode($utilisateur->getLogin()) . '">'  ." modifier". '</a></p>';
}
?>
</body>
</html>
