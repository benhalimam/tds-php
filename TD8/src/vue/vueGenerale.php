<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $titre; ?></title>
    <link rel="stylesheet" href="../ressources/css/Style.css">
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li>

            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>

            <li>
                <a href="controleurFrontal.php?action=afficherFormulairePreference">
                    <img src="../ressources/img/heart.png" alt="Cœur">
                </a>
            </li>
            <?php
            use App\Covoiturage\Lib\ConnexionUtilisateur;
            if (!ConnexionUtilisateur::estConnecte()){
                echo "<li>";
                    echo "<a href=\"controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur\">";
                        echo "<img src=\"../ressources/img/add-user.png\" alt=\"ajouter_un_utilisateur\">";
                    echo "</a>";
                echo "</li>";

                echo "<li>";
                    echo "<a href=\"controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur\">";
                        echo "<img src=\"../ressources/img/connection.png\" alt=\"se_connecter\">";
                    echo "</a>";
                echo "</li>";
            }
            else
            {
                $login = ConnexionUtilisateur::getLoginUtilisateurConnecte();
                $loginURL = htmlspecialchars($login);
                echo "<li>";
                    echo "<a href=\"controleurFrontal.php?action=afficherDetail&controleur=utilisateur&login=$loginURL\">";
                        echo "<img src=\"../ressources/img/user.png\" alt=\"detail_utilisateur\">";
                    echo "</a>";
                echo "</li>";

                echo "<li>";
                    echo "<a href=\"controleurFrontal.php?action=deconnecter&controleur=utilisateur\">";
                        echo "<img src=\"../ressources/img/logout.png\" alt=\"se_deconnecter\">";
                    echo "</a>";
                echo "</li>";
            }




                ?>
        </ul>
    </nav>


</head>
<body>
<header>
    <nav>
        <!-- Votre menu de navigation ici -->
    </nav>
</header>
<main>
    <?php
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Voila un Footer car il en faut 1 mdrrrr
    </p>
</footer>
</body>
</html>


