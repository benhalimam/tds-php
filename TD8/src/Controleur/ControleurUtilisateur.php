<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Lib\MotDePasse;
class ControleurUtilisateur extends ControleurGenerique{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        $values = [
            "utilisateurs" => $utilisateurs,
            "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/liste.php"
        ];
        self::afficherVue('vueGenerale.php',$values);
    }

    public static function afficherDetail() : void{
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if($utilisateur != null) {
            $values = [
                "utilisateur" => $utilisateur,
                "titre" => "Detail Utilisateur",
                "cheminCorpsVue" => "utilisateur/detail.php"
            ];
            self::afficherVue("vueGenerale.php",$values);
        }
        else{
            self::afficherErreur("Il n'existe aucun utilisateur de login $login");
        }
    }

    public static function afficherFormulaireCreation() : void{
        $values = [
            "titre" => "Formulaire Création",
            "cheminCorpsVue" => 'utilisateur/formulaireUtilisateur.php'
        ];
        self::afficherVue("vueGenerale.php",$values);
    }

    public static function creerDepuisFormulaire() : void{
        if ($_GET["mdp"]!=$_GET["mdp2"]){
            self::afficherErreur("Les mots de passes ne correspondent pas");
        }else {
            $utilisateur = self::construireDepuisFormulaire($_GET);
            (new UtilisateurRepository())->ajouter($utilisateur);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            $values = [
                "titre" => "Formulaire Création",
                "cheminCorpsVue" => 'utilisateur/utilisateurCree.php',
                "utilisateurs" => $utilisateurs
            ];
            self::afficherVue('vueGenerale.php', $values);
        }
    }

    public static function supprimer() : void{
        if (!isset($_GET["login"])){
            self::afficherErreur("le login de l'utilisateur est obligatoire");
            return;
        }
        $login = $_GET['login'];
        if (!ConnexionUtilisateur::estUtilisateur($login)){
            self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté");
            return;
        }
        (new UtilisateurRepository())->supprimer($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        $values = [
            "login" => "$login",
            "utilisateurs" => $utilisateurs,
            "titre" => "Votre utilisateur a été supprimé",
            "cheminCorpsVue" => 'utilisateur/utilisateurSupprime.php'
        ];
        self::afficherVue("vueGenerale.php",$values);
    }

    public static function afficherFormulaireMiseAJour(){
        $login = $_GET['login'];
        if (!ConnexionUtilisateur::estUtilisateur($login)){
            self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté");
            return;
        }
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        $values = [
            "utilisateur" => $utilisateur,
            "titre" => "Formulaire Mise A Jour",
            "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"
        ];
        self::afficherVue("vueGenerale.php",$values);
    }

    public static function mettreAJour() : void{
        if (!isset($_GET["login"]) || !isset($_GET["prenom"]) || !isset($_GET["nom"]) ||
        !isset($_GET["ancienMdp"]) || !isset($_GET["mdp"]) || !isset($_GET["mdp2"])){
            self::afficherErreur("des champs obligatoires ne sont pas remplis");
            return;
        }
        $utilisateur = self::construireDepuisFormulaire($_GET);
        if (!ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())){
            self::afficherErreur("Vous n'avez pas les permissions pour modifier un utilisateur autre que vous");
            return;
        }
        $utilisateurBD = (new UtilisateurRepository())->recupererParClePrimaire($utilisateur->getLogin());
        if ($utilisateurBD==null){
            self::afficherErreur("le login n'existe pasé");
            return;
        }
        if ($_GET["mdp"]!=$_GET["mdp2"]){
            self::afficherErreur("les nouveaux mots de passes ne correspondent pas");
            return;
        }
        if (MotDePasse::verifier($_GET["ancienMdp"],$utilisateurBD->getMdpHache())){
            self::afficherErreur("l'ancien Mot de passe est incorrect");
            return;
        }



        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        $values = [
            "titre" => "utilisateurMisAJour",
            "cheminCorpsVue" => 'utilisateur/utilisateurMisAJour.php',
            "utilisateurs" => $utilisateurs,
            "utilisateur" => $utilisateur
        ];
        self::afficherVue('vueGenerale.php',$values);
    }

    public static function afficherErreur(String $messageErreur = "") : void{
        $values = [
            "titre" => "Erreur lors de la navigation",
            "cheminCorpsVue" => 'utilisateur/erreur.php',
            "messageErreur" => $messageErreur
        ];
        self::afficherVue('vueGenerale.php',$values);
    }

    public static function afficherFormulaireConnexion(){
        $values = [
            "titre" => "Formulaire Connexion",
            "cheminCorpsVue" => "utilisateur/formulaireConnexion.php"
        ];
        self::afficherVue("vueGenerale.php",$values);
    }

    public static function connecter(): void{
        if (!isset($_GET["mdp"]) || !isset($_GET["login"])){
            self::afficherErreur("Login et/ou mot de passe manquant");
            return;
        }
        $mdpClair = $_GET["mdp"];
        $login = $_GET["login"];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if($utilisateur == null || !MotDePasse::verifier($mdpClair, $utilisateur->getMdpHache())){
            self::afficherErreur("Login et/ou mot de passe incorrect");
            return;
        }

        ConnexionUtilisateur::connecter($login);

        $values = [
            "utilisateur" => $utilisateur,
            "titre" => "Detail Utilisateur",
            "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php"
        ];
        self::afficherVue("vueGenerale.php",$values);

    }

    public static function deconnecter(): void
    {
        ConnexionUtilisateur::deconnecter();
        self::afficherListe();
    }


    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $tableauDonneesFormulaire['login'];
        $nom = $tableauDonneesFormulaire['nom'];
        $prenom = $tableauDonneesFormulaire['prenom'];
        $mdpHache = MotDePasse::hacher($tableauDonneesFormulaire['mdp']);
        $utilisateur = new Utilisateur($login, $nom, $prenom,$mdpHache);
        return $utilisateur;
    }

    public static function deposerCookie(){
        Cookie::enregistrer("cookie1","je me regale");
    }


    public static function lireCookie(){
        echo Cookie::lire("cookie1");
    }


}
?>
