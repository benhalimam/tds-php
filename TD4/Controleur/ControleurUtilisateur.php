<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::getUtilisateurs();//appel au modèle pour gérer la BD
        self::afficherVue('./../vue/utilisateur/liste.php',$utilisateurs);
    }

    public static function afficherDetail() : void{
        $login = $_GET['login'];
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login);
        if($utilisateur != null)
            self::afficherVue('./../vue/utilisateur/detail.php',[$utilisateur]);
        else
        self::afficherVue('./../vue/utilisateur/erreur.php');
    }

    public static function afficherFormulaireCreation() : void{
        self::afficherVue('./../vue/utilisateur/formulaireUtilisateur.php');
    }

    public static function creerDepuisFormulaire() : void{
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $utilisateur = new ModeleUtilisateur($login,$nom,$prenom);
        $utilisateur->ajouter();
        self::afficherListe();
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }



}
?>
