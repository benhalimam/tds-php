<?php

require_once 'ConnexionBaseDeDonnees.php';
class ModeleUtilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }



    public function __construct(string $login, string $nom, string $prenom){
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public static function construireDepuisTableauSQL(array $utilisateurTableau) : ModeleUtilisateur {
        return new ModeleUtilisateur(
            $utilisateurTableau["login"],
            $utilisateurTableau["nom"],
            $utilisateurTableau["prenom"]
        );
    }

    /**
     * @return ModeleUtilisateur[]
     */
    public static function getUtilisateurs() : array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getInstance()->getPdo()->query("SELECT * FROM utilisateur");

        $utilisateurs = [];
        foreach($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $utilisateurs;
    }

    /**
     * @throws DateMalformedStringException
     */
    public static function getTrajets(string $login): array
    {
        $sql = "SELECT * FROM trajet T JOIN passager P ON T.id = P.trajetId WHERE passagerLogin = :login";
        $valeurs = ["login" => $login];

        $pdoStatement = ConnexionBaseDeDonnees::getInstance()->getPdo()->prepare($sql);
        $pdoStatement->execute($valeurs);

        $utilisateurs = [];
        foreach($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = Trajet::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $utilisateurs;
    }


    // REQUETES PREPAREES AVEC CONDITION (WHERE)
    static public function recupererUtilisateurParLogin(string $login) : ?ModeleUtilisateur {

        $sql = "SELECT * FROM utilisateur WHERE login = :loginTag";

        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getInstance()->getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
        );

        $pdoStatement->execute($values);
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if (!$utilisateurFormatTableau) return null;
        return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter() : void{
        $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:loginTag, :nomTag, :prenomTag)";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getInstance()->getPDO()->prepare($sql);

        $values = array(
            "loginTag" => $this->login,
            "nomTag" => $this->nom,
            "prenomTag" => $this->prenom
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
    }


//    public function __toString() : string {
//        return "<p> ModeleUtilisateur {$this->prenom} {$this->nom} de login {$this->login} </p>";
//    }
}