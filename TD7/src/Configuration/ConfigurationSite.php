<?php
 namespace App\Configuration;

 class ConfigurationSite{

     static private $configuration = array(
         //temps en secondes (1H)
         'dureeExpirationSession' => 3600
     );

     public static function getDureeExpirationSession(){
         return self::$configuration['dureeExpirationSession'];
     }
 }