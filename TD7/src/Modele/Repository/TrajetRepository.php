<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Trajet;
use DateTime;

class TrajetRepository extends AbstractRepository
{

    private static function recupererPassagers(Trajet $trajet):array{
        $query = "SELECT u.login, u.nom, u.prenom FROM passager p
JOIN utilisateur u ON p.passagerLogin = u.login
 WHERE p.trajetId = :trajetIdT";
        $pdoStatement = ConnexionBaseDeDonnees::getInstance()->getPdo()->prepare($query);
        $values=array("trajetIdT"=>$trajet->getId());
        $pdoStatement->execute($values);
        $passagersTableau = $pdoStatement->fetchAll();
        $return_value=array();
        for($i = 0; $i<count($passagersTableau); $i+=1){
            $return_value[]= (new UtilisateurRepository())->construireDepuisTableauSQL($passagersTableau[$i]);
        }
        return $return_value;
    }

    public function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet= new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arriver"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"],
        );
        $trajet->setPassagers(self::recupererPassagers($trajet));
        return $trajet;
    }

//    /**
//     * @return Trajet[]
//     */
//    public static function recupererTrajets() : array {
//        $pdoStatement = ConnexionBaseDeDonnees::getInstance()->getPDO()->query("SELECT * FROM trajet");
//
//        $trajets = [];
//        foreach($pdoStatement as $trajetFormatTableau) {
//            $trajets[] = self::construireDepuisTableauSQL($trajetFormatTableau);
//        }
//
//        return $trajets;
//    }

    protected function getNomTable(): string
    {
        return "trajet";
    }

    protected function getNomClePrimaire(): string
    {
        return "id";
    }

    protected function getNomsColonnes(): array
    {
        return ["id","depart","arriver","date","prix","conducteurLogin","nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        /** @var Trajet $trajet */
        return array(
         "idTag" => $trajet->getId(),
         "departTag" => $trajet->getDepart(),
         "arriverTag" => $trajet->getArrivee(),
         "dateTag" => $trajet->getDate()->format("Y-m-d"),
         "prixTag" => $trajet->getPrix(),
         "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
         "nonFumeurTag" => $trajet->isNonFumeur()?"1":"0",
    );
    }


}