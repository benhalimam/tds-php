<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;

abstract class AbstractRepository
{
    protected abstract function getNomTable(): string;

    protected abstract function getNomClePrimaire(): string;

    protected abstract function getNomsColonnes(): array;

    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;


    public function recuperer() : array
    {
        $nomTable = $this->getNomTable();
        $pdoStatement = ConnexionBaseDeDonnees::getInstance()->getPdo()->query("SELECT * FROM $nomTable");

        $utilisateurs = [];
        foreach($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = $this->construireDepuisTableauSQL($utilisateurFormatTableau);

        }
        return $utilisateurs;
    }

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;

    public function recupererParClePrimaire(string $clePrimaire) : ?AbstractDataObject {
        $nomTable = $this->getNomTable();
        $nomClePrimaire = $this->getNomClePrimaire();
        $sql = "SELECT * FROM $nomTable WHERE $nomClePrimaire = :cleTag";

        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getInstance()->getPdo()->prepare($sql);

        $values = array(
            "cleTag" => $clePrimaire,
        );

        $pdoStatement->execute($values);
        $objetFormatTableau = $pdoStatement->fetch();

        if (!$objetFormatTableau) return null;
        return $this->construireDepuisTableauSQL($objetFormatTableau);
    }

    public function supprimer($valeurClePrimaire){
        $nomTable = $this->getNomTable();
        $nomClePrimaire = $this->getNomClePrimaire();
        $sql = "DELETE FROM $nomTable WHERE $nomClePrimaire = :cleTag";

        $pdoStatement = ConnexionBaseDeDonnees::getInstance()->getPdo()->prepare($sql);

        $values = array(
            "cleTag" => $valeurClePrimaire,
        );

        $pdoStatement->execute($values);
    }

    public function ajouter(AbstractDataObject $objet) : bool{
        $nomTable = $this->getNomTable();
        $stringColonnes = $this->getStringColonnes();
        $stringValues = $this->getStringValues();

        $sql = "INSERT INTO $nomTable $stringColonnes VALUES $stringValues";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getInstance()->getPDO()->prepare($sql);

        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($this->formatTableauSQL($objet));
        return true;
    }

    public function mettreAJour(AbstractDataObject $objet) : void{

        $nomTable = $this->getNomTable();
        $stringSet = $this->getStringSet();
        $clePrimaire = $this->getNomClePrimaire();

        $sql = "UPDATE $nomTable set $stringSet WHERE $clePrimaire = :"."$clePrimaire"."Tag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getInstance()->getPDO()->prepare($sql);
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($this->formatTableauSQL($objet));
    }

    private function getStringColonnes():string{
        $str = "(";
        $tableau = $this->getNomsColonnes();
        $taille = count($tableau);

        for ($i=0; $i < $taille; $i++) {
            $str .= $tableau[$i];
            if ($i < $taille - 1) {
                $str .= ", ";
            }
        }

        return $str.")";
    }

    private function getStringValues():string{
        $str = "(";
        $tableau = $this->getNomsColonnes();
        $taille = count($tableau);

        for ($i=0; $i < $taille; $i++) {
            $str .= ":".$tableau[$i]."Tag";
            if ($i < $taille - 1) {
                $str .= ", ";
            }
        }

        return $str.")";
    }

    private function getStringSet():string{
        $str = "";
        $tableau = $this->getNomsColonnes();
        $taille = count($tableau);

        for ($i=0; $i < $taille; $i++) {
            $current = $tableau[$i];
            $str .= $current."=:".$current."Tag";
            if ($i < $taille - 1) {
                $str .= ", ";
            }
        }

        return $str;
    }







}