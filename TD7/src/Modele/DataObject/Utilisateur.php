<?php

namespace App\Covoiturage\Modele\DataObject;

class Utilisateur  extends AbstractDataObject {

    private string $login;
    private string $nom;
    private string $prenom;


    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }






    public function __construct(string $login, string $nom, string $prenom){
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }




//    public function __toString() : string {
//        return "<p> Utilisateur {$this->prenom} {$this->nom} de login {$this->login} </p>";
//    }
}