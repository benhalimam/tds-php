<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title> Mon premier php </title>
</head>
<body>
<form method="get" action="controleurFrontal.php">
    <input type='hidden' name='action' value='enregistrerPreference'>
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur"
            <?= \App\Covoiturage\Lib\PreferenceControleur::lire()=="utilisateur"? "checked": ""?>>
        <label for="utilisateurId">Utilisateur</label>
        <input type="radio" id="trajetId" name="controleur_defaut" value="trajet"
            <?= \App\Covoiturage\Lib\PreferenceControleur::lire()=="trajet"? "checked": ""?>>
        <label for="trajetId">Trajet</label>
        <input type="submit" value="Envoyer" />
    </fieldset>
</form>
</body>
</html>

