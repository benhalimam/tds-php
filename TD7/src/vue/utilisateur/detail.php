<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php
/** @var Utilisateur[] $parametres */
echo '<p> Utilisateur '.htmlspecialchars($parametres["utilisateur"]->getNom()).' '.htmlspecialchars($parametres["utilisateur"]->getPrenom()).' de login '.htmlspecialchars($parametres["utilisateur"]->getLogin()).' </p>';
?>
</body>
</html>
