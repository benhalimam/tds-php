<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title> Mon premier php </title>
</head>
<body>
<form method="get" action="controleurFrontal.php">
    <input type='hidden' name='action' value='mettreAJour'>
    <input type='hidden' name='controleur' value='utilisateur'>
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label for="login_id" class="InputAddOn-item">Login</label> :
            <input class="InputAddOn-field"  type="text" value=<?= htmlspecialchars($parametres["utilisateur"]->getLogin())?>
            name="login" id="login_id" readonly/>
        </p>
        <p class="InputAddOn">
            <label for="prenom_id" class="InputAddOn-item">Prenom</label> :
            <input class="InputAddOn-field"  type="text" value=<?= htmlspecialchars($parametres["utilisateur"]->getPrenom())?> name="prenom" id="prenom_id" required />
        </p>
        <p class="InputAddOn">
            <label for="nom_id" class="InputAddOn-item">Nom</label> :
            <input class="InputAddOn-field" type="text" value=<?= htmlspecialchars($parametres["utilisateur"]->getNom())?> name="nom" id="nom_id" required />
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
</body>
</html>
